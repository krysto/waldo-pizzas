const express = require('express');
const compress = require('compression');

// Setup server
const app = express();
app.use(compress());

// // Serve bundled js and css as static resources
app.use('/', express.static(`${__dirname}/build`));

const port = process.env.PORT || 80;

// Start the server
app.listen(port, () => console.log(`Server listening on port ${port}`) );