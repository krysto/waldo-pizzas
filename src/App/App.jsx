import React from 'react';
import OrderForm from './OrderForm';
import Cart from './Cart';

import './common.css';

export default function App() {
	return (
		<div>
			<OrderForm />
			<Cart />
		</div>
	);
};