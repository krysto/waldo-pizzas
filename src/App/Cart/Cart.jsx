import React, { PropTypes } from 'react';
import get from 'get-deep';
import CSSModules from 'react-css-modules';

import Pizza, { calcCost } from './Pizza';

import styles from './Cart.css';

const calcTotalCost = pizzas => pizzas.reduce(
	(total, current) => total + calcCost(current), 0
);

function Cart({ pizzas }) {
	return !!get(pizzas, 'length') && (
		<div>
			<h2>Your cart</h2>
			<ul>
				{pizzas.map((pizza, i) =>
					<Pizza key={i} pizza={pizza} />
				)}
			</ul>
			<div styleName="total">
				<span>Total Cost: </span>
				<span>
					${calcTotalCost(pizzas)}
				</span>
			</div>
		</div>
	);
};

Cart.PropTypes = {
	pizzas: PropTypes.arrayOf(
		PropTypes.object
	).isRequired,
};

export default CSSModules(Cart, styles);