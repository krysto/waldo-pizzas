import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Cart from './Cart';

export default connect(
	createStructuredSelector({
		pizzas: state => state.selectedPizzas,
	}),
)(Cart);