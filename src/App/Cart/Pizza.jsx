import CSSModules from 'react-css-modules';
import React, { PropTypes } from 'react';

import styles from './Pizza.css';

export const calcCost = pizza => pizza.basePrice + pizza.toppings.reduce(
	(total, current) => total + current.price, 0
);

function Pizza({ pizza }) {
	return (
		<div styleName="root">
			<div>
				Size {pizza.size} - ${pizza.basePrice}
			</div>
			{pizza.toppings.length > 0 &&
				<div>
					<div>With Toppings</div>
					<ul>
						{pizza.toppings.map(topping =>
							<li key={topping.name}>
								{topping.name} - ${topping.price}
							</li>
						)}
					</ul>
				</div>
			}
			<div>
				<strong>
					Price: ${calcCost(pizza)}
				</strong>
			</div>
		</div>
	);
};

Pizza.propTypes = {
	pizza: PropTypes.object,
};

export default CSSModules(Pizza, styles);