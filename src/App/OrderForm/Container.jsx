import { connect } from 'react-redux';
import { createStructuredSelector, createSelector } from 'reselect';
import * as actionCreators from 'shared/actionCreators';

import OrderForm from './OrderForm';

const mapStateToProps = createStructuredSelector({
	selectedSize: createSelector(
		(state, props) => props.pizzaSizes,
		(state) => state.orderForm.size,
		(allSizes, selectedSizeName) => allSizes && allSizes.find(size => size.name === selectedSizeName)
	),
	selectedToppingNames: state => state.orderForm.toppings,
});

const usedActionCreators = {
	addPizza: actionCreators.addPizza,
};

export default connect(mapStateToProps, usedActionCreators)(OrderForm);