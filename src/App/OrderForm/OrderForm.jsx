import React, { PropTypes } from 'react';
import CSSModules from 'react-css-modules';

import SizeSelect from './SizeSelect';
import ToppingSelect from './ToppingSelect';

import styles from './OrderForm.css';

class OrderForm extends React.PureComponent {
	static propTypes = {
		pizzaSizes: PropTypes.array,
		selectedSize: PropTypes.object,
		selectedToppingNames: PropTypes.array,
		addPizza: PropTypes.func.isRequired,
	};

	static defaultProps = {
		selectedToppingNames: [],
	};

	render() {
		const { pizzaSizes, selectedSize, selectedToppingNames } = this.props;

		return !!pizzaSizes && (
			<div>
				<h2>Customize your pizza</h2>
				<form onSubmit={this._handleSubmit}>
					<SizeSelect pizzaSizes={pizzaSizes} />
					{!!selectedSize &&
						<ToppingSelect 
							toppings={selectedSize.toppings} 
							maxToppings={selectedSize.maxToppings}
						/>
					}
					<button 
						type="submit" 
						styleName="submit"
						disabled={!selectedSize || !selectedToppingNames.length}
					>
						ADD TO CART
					</button>
				</form>
			</div>
		);
	}

	_handleSubmit = event => {
		event.preventDefault();

		const { selectedSize, selectedToppingNames, addPizza } = this.props;
		const pizza = {
			size: selectedSize.name,
			basePrice: selectedSize.basePrice,
			toppings: selectedSize.toppings
				.filter(t => selectedToppingNames.indexOf(t.topping.name) >= 0)
				.map(t => t.topping),
		};
		addPizza(pizza);
	};
};

export default CSSModules(OrderForm, styles);