import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as actionCreators from 'shared/actionCreators';

import SizeSelect from './SizeSelect';

const mapStateToProps = createStructuredSelector({
	selectedSizeName: state => state.orderForm.size,
});

const usedActionCreators = {
	selectSize: actionCreators.selectSize,
}

export default connect(mapStateToProps, usedActionCreators)(SizeSelect);