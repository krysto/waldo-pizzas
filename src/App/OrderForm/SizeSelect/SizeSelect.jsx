import React, { PropTypes } from 'react';

export default class SizeSelect extends React.PureComponent {
	static propTypes = {
		pizzaSizes: PropTypes.array.isRequired,
		selectedSizeName: PropTypes.string,
		selectSize: PropTypes.func.isRequired,
	}

	render() {
		const { pizzaSizes, selectedSizeName } = this.props;

		return (
			<div>
				<p>Select your pizza size</p>
				{pizzaSizes.map(size =>
					<p key={size.name}>
						<label>
							<input 
								type="radio"
								name="pizza-size-select" 
								value={size.name}
								checked={size.name === selectedSizeName}
								onChange={this._handleChange}
							/>
								{size.name} - ${size.basePrice}
						</label>
					</p>
				)}
			</div>
		);
	}

	_handleChange = event => {
		const { pizzaSizes, selectSize } = this.props;
		const selectedSize = pizzaSizes.find(size => size.name === event.target.value);
		selectSize(selectedSize);
	};
};