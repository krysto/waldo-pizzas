import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as actionCreators from 'shared/actionCreators';

import ToppingSelect from './ToppingSelect';

const mapStateToProps = createStructuredSelector({
	selectedToppingNames: state => state.orderForm.toppings,
});

const usedActionCreators = {
	selectTopping: actionCreators.selectTopping,
	deselectTopping: actionCreators.deselectTopping,
};

export default connect(mapStateToProps, usedActionCreators)(ToppingSelect);