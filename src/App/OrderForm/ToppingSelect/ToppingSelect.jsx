import React, { PropTypes } from 'react';

export default class ToppingSelect extends React.PureComponent {
	static propTypes = {
		toppings: PropTypes.array.isRequired,
		maxToppings: PropTypes.number,
		selectedToppingNames: PropTypes.array,
		selectTopping: PropTypes.func.isRequired,
		deselectTopping: PropTypes.func.isRequired,
	};

	static defaultProps = {
		selectedToppingNames: [],
	};

	render() {
		const { toppings, maxToppings, selectedToppingNames } = this.props;

		return (
			<div>
				<p>Select your topping</p>
				<ul>
					{toppings.map(({ topping }) => {
						const selected = selectedToppingNames.indexOf(topping.name) >= 0;

						return (
							<li key={topping.name}>
								<label>
									<input 
										type="checkbox" 
										value={topping.name} 
										checked={selected}
										onChange={this._handleChange} 
										disabled={maxToppings && !selected && selectedToppingNames.length >= maxToppings}
									/>
									&nbsp;{topping.name} - ${topping.price}
								</label>
							</li>
						);
					})}
				</ul>
			</div>
		);
	}

	_handleChange = event => {
		if (event.target.checked) {
			this.props.selectTopping(event.target.value);
		} else {
			this.props.deselectTopping(event.target.value);
		}
	};
};