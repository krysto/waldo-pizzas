import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import Container from './Container';

const query = gql`
	query getAllPizzaSizes {
		pizzaSizes {
			name,
			basePrice,
			maxToppings,
			toppings {
				topping {
					name,
					price
				},
				defaultSelected
			}
		}
	}
`;

const options = { 
	props: ({ ownProps, data: { pizzaSizes } }) => ({
		pizzaSizes
	}) 
};

export default graphql(query, options)(Container);