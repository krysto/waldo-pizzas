import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient, { createNetworkInterface } from 'apollo-client';

import createStore from './store';

import { ApolloProvider } from 'react-apollo';
import App from './App';

// Setup Apollo
const apolloClient = new ApolloClient({
	networkInterface: createNetworkInterface({ uri: 'https://core-graphql.dev.waldo.photos/pizza' }),
});

// Setup Redux's store
const store = createStore({}, apolloClient);

// Render the app
ReactDOM.render(
	<ApolloProvider store={store} client={apolloClient}>
		<App />
	</ApolloProvider>,
	document.getElementById('root')
);
