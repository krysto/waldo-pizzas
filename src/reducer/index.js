import { combineReducers } from 'redux';
import orderForm from './orderForm';
import selectedPizzas from './selectedPizzas';

export default function createReducer(apolloClient) {
	return combineReducers({
		orderForm,
		selectedPizzas,
		apollo: apolloClient.reducer(),
	});
}