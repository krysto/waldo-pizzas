import { combineReducers } from 'redux';
import size from './size';
import toppings from './toppings';

export default combineReducers({
	size,
	toppings,
});