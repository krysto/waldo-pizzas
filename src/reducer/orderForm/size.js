// Type: String
const initialState = null;

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case 'SELECT_SIZE':
			return payload.name;
		default:
			return state;
	}
}