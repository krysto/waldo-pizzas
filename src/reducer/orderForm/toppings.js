// Type: [String]
const initialState = [];

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case 'SELECT_SIZE':
			return payload.toppings
				.filter(t => t.defaultSelected)
				.map(t => t.topping.name);
		case 'SELECT_TOPPING':
			return [
				...state,
				payload,
			];
		case 'DESELECT_TOPPING':
			const removeIndex = state.indexOf(payload);

			if (removeIndex >= 0) {
				const newState = [...state];
				newState.splice(removeIndex, 1);

				return newState;
			} else {
				return state;
			}
		default:
			return state;
	}
}