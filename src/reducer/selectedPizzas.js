// Type: [{
// 	size,
// 	basePrice,
// 	toppings [{
// 		name,
// 		price
// 	}]
// }]
const initialState = [];

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case 'ADD_PIZZA':
			return [
				...state,
				payload,
			];
		default:
			return state;
	}
}