export const selectSize = size => ({
	type: 'SELECT_SIZE',
	payload: size,
});

export const selectTopping = toppingName => ({
	type: 'SELECT_TOPPING',
	payload: toppingName,
});

export const deselectTopping = toppingName => ({
	type: 'DESELECT_TOPPING',
	payload: toppingName,
});

export const addPizza = (pizza) => ({
	type: 'ADD_PIZZA',
	payload: pizza,
});