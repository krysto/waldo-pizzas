import { applyMiddleware, createStore } from 'redux';
import createLogger from 'redux-logger';
import createReducer from './reducer';

export default function(initialState, apolloClient) {
	const middlewares = [apolloClient.middleware()];
	if (process.env.NODE_ENV === 'development') {
		middlewares.push(createLogger());
	}

	const storeEnhancer = applyMiddleware(...middlewares);
	const createStoreWithMiddlewares = storeEnhancer(createStore);

	const reducer = createReducer(apolloClient);

	const store = createStoreWithMiddlewares(reducer, initialState);

	return store;
}